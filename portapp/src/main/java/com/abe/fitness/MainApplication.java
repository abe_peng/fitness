package com.abe.fitness;

import com.abe.libfitness.base.ComApplication;
import com.abe.libfitness.utils.SpFitnessHelper;

public class MainApplication extends ComApplication {
    @Override
    public void onCreate() {
        SpFitnessHelper.screen(true);
        super.onCreate();
    }
}

package com.abe.libfitness.main;

import android.annotation.SuppressLint;
import android.util.Log;
import android.widget.TextView;

import com.abe.libfitness.R;
import com.abe.libfitness.R2;
import com.abe.libfitness.base.BaseComTempActivity;
import com.abe.libfitness.entity.ComReceiveDataBean;
import com.abe.libfitness.utils.CMDConts;
import com.abe.libfitness.utils.DataCovertUtil;
import com.abe.libfitness.utils.SpFitnessHelper;
import com.abe.libfitness.widget.CircleChartView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

@SuppressLint("ResourceType")
public class DetectionActivity extends BaseComTempActivity {
    @BindView(R2.id.myview_circle_chart)
    CircleChartView mDialChartView;
    @BindView(R2.id.jili_test)
    TextView mJiLiTest;
    @BindView(R2.id.juli_test)
    TextView mJuLiTest;

    /**
     * 用于界面显示的变量
     **/
    private int chartViewStrengthValue = 0;
    private int chartViewMaxStrengthValue = 0;
    private double chartViewDistanceValue = 0;
    private double chartViewMaxDistanceValue = 0;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_detection;
    }

    @Override
    protected void initContent() {

    }

    @OnClick(R2.id.iv_back)
    public void back() {
        SpFitnessHelper.strength(MAX_STRENGTH);
        SpFitnessHelper.distance(MAX_DISTANCE);
        MAX_STRENGTH = 0;
        MAX_DISTANCE = 0;
        finish();
    }

    @OnClick(R2.id.jili_test)
    public void strength() {
        if (!mJiLiTest.getText().toString().equals("检测中...")) {
            mDialChartView.setId(1);
            mJiLiTest.setText(R.string.runing);
            mJuLiTest.setText(R.string.jl_detection);
            addMuscleStrengthTestData();
        }
    }

    @OnClick(R2.id.juli_test)
    public void distance() {
        if (!mJuLiTest.getText().toString().equals("检测中...")) {
            mDialChartView.setId(2);
            mJuLiTest.setText(R.string.runing);
            mJiLiTest.setText(R.string.jl_test);
            addDistanceDetectData();
        }
    }

    /**
     * 添加肌力测试数据
     */
    private void addMuscleStrengthTestData() {
        closeSendThread();
        List<byte[]> cmdList = new ArrayList<>();
        cmdList.add(CMDConts.CMD_WRITE_F_REIGSTER_F0);
        cmdList.add(CMDConts.CMD_WRITE_F_REIGSTER_F1);
        cmdList.add(CMDConts.CMD_WRITE_F_REIGSTER_F2);
        cmdList.add(CMDConts.CMD_WRITE_F_REIGSTER_F3);
        cmdList.add(CMDConts.CMD_WRITE_F_REIGSTER_F4);
        cmdList.add(CMDConts.CMD_WRITE_F_REIGSTER_F5);
        cmdList.add(CMDConts.CMD_WRITE_F_REIGSTER_D0);
        cmdList.add(CMDConts.CMD_WRITE_F_REIGSTER_D1);
        cmdList.add(CMDConts.CMD_WRITE_F_REIGSTER_D2);
        cmdList.add(CMDConts.CMD_WRITE_F_REIGSTER_D3);
        cmdList.add(CMDConts.CMD_WRITE_F_REIGSTER_D4);
        cmdList.add(CMDConts.CMD_WRITE_F_REIGSTER_D5);
        cmdList.add(CMDConts.CMD_READ_REGISTER_3);
        // 开启发送数据
        mSendThread = new SendThread(cmdList, false);
        mSendThread.start();
    }

    /**
     * 添加距离检测数据
     */
    private void addDistanceDetectData() {
        closeSendThread();
        List<byte[]> cmdList = new ArrayList<>();
        cmdList.add(CMDConts.CMD_WRITE_D_REIGSTER_F0);
        cmdList.add(CMDConts.CMD_WRITE_D_REIGSTER_F1);
        cmdList.add(CMDConts.CMD_WRITE_D_REIGSTER_F2);
        cmdList.add(CMDConts.CMD_WRITE_D_REIGSTER_F3);
        cmdList.add(CMDConts.CMD_WRITE_D_REIGSTER_F4);
        cmdList.add(CMDConts.CMD_WRITE_D_REIGSTER_F5);
        cmdList.add(CMDConts.CMD_READ_REGISTER_4);
        mSendThread = new SendThread(cmdList, false);
        mSendThread.start();
    }

    @Override
    public void onDataReceived(ComReceiveDataBean ComRecData) {
        if (ComRecData.verification()) {
            // 接收到命令发送过来的数据
            final int msg = DataCovertUtil.HexToInt(DataCovertUtil.ByteArrToHex(ComRecData.bRec).substring(6, 10));
            final int control = DataCovertUtil.HexToInt(DataCovertUtil.ByteArrToHex(ComRecData.bRec).substring(2, 4));
            Log.d(TAG, "onDataReceived Read:" + DataCovertUtil.ByteArrToHex(ComRecData.bRec));
            if (control == 3) {// 3表示：读
                // 瞬时值初始化单位牛顿
                int instantaneousValueN = msg;
                int chartViewID = mDialChartView.getId();
                switch (chartViewID) {
                    case 1: {
                        // 牛顿转换为公斤
                        int instaneousValueKg = (int) 1.0 * instantaneousValueN * 10 / 333;
                        Log.d(TAG, "onDataReceived instantaneousValueN:" + instantaneousValueN + ",instaneousValueKg:" + instaneousValueKg);

                        Log.d(TAG, "------>max:" + MAX_STRENGTH);
                        if (instaneousValueKg > MAX_STRENGTH) {// 实时值大于保存的最大值
                            // 保存最大值
                            MAX_STRENGTH = instaneousValueKg;
                        }
                        // 仪表盘数据
                        chartViewStrengthValue = (int) (instaneousValueKg * 100 / 100);
                        chartViewMaxStrengthValue = (int) (MAX_STRENGTH * 100 / 100);
                        Log.d(TAG, "onDataReceived chartViewStrengthValue:" + chartViewStrengthValue);
                        // 更新UI
                        runOnUiThread(new Runnable() {
                            public void run() {
                                mDialChartView.setPercent(chartViewStrengthValue, chartViewMaxStrengthValue);
                                mDialChartView.setText(String.valueOf(chartViewStrengthValue));
                            }
                        });
                        break;
                    }
                    case 2: {
                        if (instantaneousValueN > MAX_DISTANCE) {// 实时值大于保存的最大值
                            MAX_DISTANCE = instantaneousValueN;
                            // 保存最大值
                            Log.d(TAG, "----->instantaneousValueN:" + instantaneousValueN);
                        }

                        // 仪表盘数据
                        chartViewDistanceValue = instantaneousValueN * 100 / 3500;
                        chartViewMaxDistanceValue = MAX_DISTANCE * 100 / 3500;
                        Log.d(TAG, "onDataReceived chartViewDistanceValue:" + chartViewDistanceValue + ",maxValue:" + chartViewMaxDistanceValue);
                        runOnUiThread(new Runnable() {
                            public void run() {
                                mDialChartView.setPercent(chartViewDistanceValue, chartViewMaxDistanceValue);
                                mDialChartView.setText(String.valueOf((int) chartViewDistanceValue));
                            }
                        });
                        break;
                    }
                    default:
                        break;
                }
            }
        }
    }
}
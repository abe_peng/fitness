package com.abe.libfitness.main.dialog;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.abe.libfitness.R;
import com.abe.libfitness.base.BaseScreenDialog;

import org.jetbrains.annotations.NotNull;

public class MotorDialog extends BaseScreenDialog implements View.OnClickListener {
    private boolean motorStart;
    private OnMotorActionListener actionListener;

    public MotorDialog(@NotNull Context context, boolean motorStart, OnMotorActionListener listener) {
        super(context);
        this.motorStart = motorStart;
        this.actionListener = listener;
    }

    @Override
    protected Boolean isAutoZoom() {
        return true;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.item_dialog_motor;
    }

    @Override
    protected void initContent() {
        TextView content = findViewById(R.id.item_content);
        content.setText(motorStart ? R.string.motor_start_confirm : R.string.motor_stop_confirm);
        findViewById(R.id.item_cancel).setOnClickListener(this);
        findViewById(R.id.item_confirm).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        boolean confirm = v.getId() != R.id.item_cancel;
        dismiss();
        if (actionListener == null) return;
        actionListener.onAction(motorStart, confirm);
    }

    public interface OnMotorActionListener {
        void onAction(boolean motorStart, boolean confirm);
    }
}
package com.abe.libfitness.main.program;

import android.os.Bundle;
import android.widget.SeekBar;
import android.widget.TextView;

import com.abe.libfitness.R;
import com.abe.libfitness.R2;
import com.abe.libfitness.base.BaseActivity;
import com.abe.libfitness.entity.FitnessCustomEntity;
import com.abe.libfitness.utils.ConstantFitness;

import butterknife.BindView;
import butterknife.OnClick;

public class FitnessCustomSetActivity extends BaseActivity implements SeekBar.OnSeekBarChangeListener {
    @BindView(R2.id.item_title)
    TextView itemTitle;
    @BindView(R2.id.report_number)
    TextView mTargetValue1;
    @BindView(R2.id.report_time)
    TextView mTargetValue2;
    @BindView(R2.id.report_calorie)
    TextView mTargetValue3;


    private int mWeight1 = 10;
    private int mWeight2 = 20;
    private int mWeight3 = 30;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_fitness_custom_set;
    }

    @Override
    protected void initContent() {
        mTargetValue1.setText(String.valueOf(mWeight1));
        mTargetValue2.setText(String.valueOf(mWeight2));
        mTargetValue3.setText(String.valueOf(mWeight3));
    }

    @OnClick(R2.id.iv_back)
    public void back() {
        finish();
    }

    @OnClick(R2.id.ok)
    public void commit() {
        Bundle bundle = new Bundle();
        bundle.putParcelable(ConstantFitness.TARGET, new FitnessCustomEntity(mWeight1, mWeight2, mWeight3));
        gotoActivity(FitnessCustomActivity.class, bundle);
        finish();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        int id = seekBar.getId();
        if (id == R.id.sb_target1) {
            mWeight1 = progress;
            mTargetValue1.setText(String.valueOf(progress));
        } else if (id == R.id.sb_target2) {
            mWeight2 = progress;
            mTargetValue2.setText(String.valueOf(progress));
        } else if (id == R.id.sb_target3) {
            mWeight3 = progress;
            mTargetValue3.setText(String.valueOf(progress));
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}

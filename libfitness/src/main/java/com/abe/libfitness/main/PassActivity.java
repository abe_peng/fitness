package com.abe.libfitness.main;

import android.widget.EditText;

import com.abe.libfitness.R;
import com.abe.libfitness.R2;
import com.abe.libfitness.base.BaseActivity;
import com.abe.libfitness.utils.SpFitnessHelper;

import butterknife.BindView;
import butterknife.OnClick;

public class PassActivity extends BaseActivity {
    @BindView(R2.id.login_pwd)
    EditText loginPwd;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_pass;
    }

    @Override
    protected void initContent() {

    }

    @OnClick(R2.id.iv_back)
    public void back() {
        gotoActivity(MainActivity.class);
        finish();
    }

    @OnClick(R2.id.login_ok)
    public void login() {
        String password = loginPwd.getText().toString();
        if (SpFitnessHelper.password().equals(password)) {
            gotoActivity(SettingActivity.class);
            finish();
        } else {
            toast(R.string.log_file);
        }
    }
}
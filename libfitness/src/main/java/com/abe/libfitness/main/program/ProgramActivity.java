package com.abe.libfitness.main.program;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

import com.abe.libfitness.R;
import com.abe.libfitness.R2;
import com.abe.libfitness.base.BaseActivity;

import butterknife.BindView;
import butterknife.OnClick;

public class ProgramActivity extends BaseActivity {
    @BindView(R2.id.muscle_model_img)
    ImageView muscleModelImg;
    @BindView(R2.id.fit_model_img)
    ImageView fitModelImg;
    @BindView(R2.id.lose_model_img)
    ImageView loseModelImg;
    @BindView(R2.id.custom_model_img)
    ImageView customModelImg;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_program;
    }

    @Override
    protected void initContent() {
        float imgScale = (float) 526 / (float) 539;
        //监听整个布局
        ViewTreeObserver vto = muscleModelImg.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                muscleModelImg.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                ViewGroup.LayoutParams params = muscleModelImg.getLayoutParams();
                int height = (int) (params.width * imgScale);
                params.height = height;
                muscleModelImg.setLayoutParams(params);
                params = fitModelImg.getLayoutParams();
                params.height = height;
                fitModelImg.setLayoutParams(params);
                params = loseModelImg.getLayoutParams();
                params.height = height;
                loseModelImg.setLayoutParams(params);
                params = customModelImg.getLayoutParams();
                params.height = height;
                customModelImg.setLayoutParams(params);
            }
        });
    }

    @OnClick({R2.id.iv_back, R2.id.muscle_model, R2.id.fit_model, R2.id.lose_model, R2.id.custom_model})
    public void onViewClicked(View view) {
        int id = view.getId();
        if (id == R.id.iv_back) {
            finish();
        } else if (id == R.id.muscle_model) {
            gotoActivity(AddMuscleActivity.class);
        } else if (id == R.id.fit_model) {
            gotoActivity(BodyBuildingActivity.class);
        } else if (id == R.id.lose_model) {
            gotoActivity(LossFatActivity.class);
        } else if (id == R.id.custom_model) {
            gotoActivity(FitnessCustomSetActivity.class);
        }
    }
}
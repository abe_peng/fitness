package com.abe.libfitness.main;

import android.text.TextUtils;
import android.widget.TextView;

import com.abe.libfitness.R;
import com.abe.libfitness.R2;
import com.abe.libfitness.entity.TrainDataEntity;
import com.abe.libfitness.utils.ConstantFitness;
import com.abe.libfitness.utils.DataCovertUtil;
import com.abe.libfitness.utils.SpFitnessHelper;
import com.abe.libquick.utils.quick.EmptyUtils;

import java.util.Calendar;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.OnClick;

public class TrainingActivity extends BaseExerciseActivity {
    private static final int WeightMax = 100, WeightMin = 5;
    @BindView(R2.id.tv_target_training)
    TextView tvTargetTraining;
    @BindView(R2.id.tv_target_unit)
    TextView tvTargetUnit;
    @BindView(R2.id.tv_target_training_unit)
    TextView tvTargetTrainingUnit;
    @BindView(R2.id.tv_target_showtime)
    TextView tvTargetShowtime;
    @BindView(R2.id.tv_target_showweight)
    TextView tvTargetShowWeight;
    @BindView(R2.id.text_show_number)
    TextView textShowNumber;
    @BindView(R2.id.text_show_calorie)
    TextView textShowCalorie;
    @BindView(R2.id.tv_pause)
    TextView tvPause;
    @BindView(R2.id.tv_restart)
    TextView tvRestart;
    private String flag;
    //计算器
    private Calendar mCalendar = Calendar.getInstance();
    private boolean firstTime = true;
    private boolean startTime = false;
    private Timer timer;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_training;
    }

    @Override
    protected void initExercise() {
        if (EmptyUtils.isEmpty(bundle()) || !bundle().containsKey(ConstantFitness.FLAG)) {
            finish();
            return;
        }
        flag = bundle().getString(ConstantFitness.FLAG);
        if (TextUtils.isEmpty(flag)) {
            finish();
            return;
        }
        int target = bundle().getInt(ConstantFitness.TARGET, 0);
        mShowWeight = 10;
        tvTargetTraining.setText(String.valueOf(target));
        switch (flag) {
            case ConstantFitness.NUMBER:
                resetCalendar();
                tvTargetUnit.setText(R.string.number_uint);
                tvTargetTrainingUnit.setText(R.string.target_number_show);
                break;
            case ConstantFitness.TIME:
                tvTargetUnit.setText(R.string.time_uint);
                tvTargetTraining.setText(R.string.target_time_show);
                break;
            case ConstantFitness.CALORIE:
                resetCalendar();
                tvTargetUnit.setText(R.string.calorie_uint);
                tvTargetTraining.setText(R.string.target_calorie_show);
                break;
        }
        showClock(tvTargetShowtime);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        closeTimer();
    }

    @OnClick(R2.id.iv_back)
    public void back() {
        TrainDataEntity dataEntity = new TrainDataEntity();
        dataEntity.setNumber(mRealCounts);
        dataEntity.setTime(mCalendar.get(Calendar.MINUTE));
        dataEntity.setCalorie(calorie);
        SpFitnessHelper.train(dataEntity);
        gotoActivity(ReportActivity.class);
        finish();
    }

    @OnClick(R2.id.show_pause)
    public void pause() {
        if (!"00:00".equals(tvTargetShowtime.getText().toString())) {
            int minute = mCalendar.get(Calendar.MINUTE);
            int second = mCalendar.get(Calendar.SECOND);
            closeTimer();
            mCalendar.set(Calendar.HOUR_OF_DAY, 0);
            mCalendar.set(Calendar.MINUTE, minute);
            mCalendar.set(Calendar.SECOND, second);
            startTime = true;
        }
    }

    @OnClick(R2.id.show_restart)
    public void restart() {
        if (!"00:00".equals(tvTargetShowtime.getText().toString())) {
            resetCalendar();
            closeTimer();
            mRealCounts = 0;
            calorie = 0;
            textShowNumber.setText(String.valueOf(mRealCounts));
            textShowCalorie.setText(String.format(Locale.CHINA, "%.1f", calorie));
            tvTargetShowtime.setText(R.string.init_time);
            firstTime = true;
        }
        showClock(tvTargetShowtime);
    }

    @OnClick(R2.id.zl_zj_10)
    public void plus10() {
        int gear = 10;
        if (mShowWeight < WeightMax) {
            if (mShowWeight + gear >= WeightMax) {
                mShowWeight = WeightMax;
            } else {
                mShowWeight = mShowWeight + gear;
            }
            tvTargetShowWeight.setText(String.valueOf(mShowWeight));
        } else {
            toast(R.string.max_weight);
        }
    }

    @OnClick(R2.id.zl_js_10)
    public void reduce10() {
        int gear = 10;
        if (mShowWeight > WeightMin) {
            if (mShowWeight > gear) {
                mShowWeight = mShowWeight - gear;
            } else {
                mShowWeight = 1;
            }
            tvTargetShowWeight.setText(String.valueOf(mShowWeight));
        } else {
            toast(R.string.min_weight);
        }
    }

    @OnClick(R2.id.zl_zj_1)
    public void plus1() {
        if (mShowWeight < WeightMax) {
            mShowWeight++;
            tvTargetShowWeight.setText(String.valueOf(mShowWeight));
        } else {
            toast(R.string.max_weight);
        }
    }

    @OnClick(R2.id.zl_js_1)
    public void reduce1() {
        if (mShowWeight > WeightMin) {
            mShowWeight--;
            tvTargetShowWeight.setText(String.valueOf(mShowWeight));
        } else {
            toast(R.string.min_weight);
        }
    }

    @Override
    protected void onDistance() {
        if (dataEntity.getDistance() > 150) {
            if (firstTime) {
                resetCalendar();
                showClock(tvTargetShowtime);
                firstTime = false;
            }
            if (startTime) {
                showClock(tvTargetShowtime);
                startTime = false;
            }
        }
        if (dataEntity.getDistance() >= 1500) { // 检测到距离大于60%最大距离时
            mStatus = 1; // 拉伸状态变为1
            if (distance4Calorie < dataEntity.getDistance()) {
                distance4Calorie = dataEntity.getDistance();
            }
        }
        // 拉伸状为1并且 检测到距离小于30%最大距离时拉伸状态变为
        if (mStatus == 1 && dataEntity.getDistance() < 1500) {
            mRealCounts++; // 次数 + 1
            mStatus = 0; // 拉伸状态清除
            if (distance4Calorie >= 1500 && distance4Calorie < 2000) {
                calorie = calorie + (dataEntity.getPower() / (180 + (Math.random() * 40)));
            } else if (distance4Calorie >= 2000 && distance4Calorie < 3000) {
                calorie = calorie + (dataEntity.getPower() / (130 + (Math.random() * 40)));
            } else {
                calorie = calorie + (dataEntity.getPower() / (80 + (Math.random() * 40)));
            }
        }
        switch (flag) {
            case ConstantFitness.NUMBER:
                if (mRealCounts == Integer.parseInt(tvTargetTraining.getText().toString())) {
                    toast(R.string.finish);
                }
                break;
            case ConstantFitness.TIME:
                if (mCalendar.get(Calendar.MINUTE) == Integer.parseInt(tvTargetTraining.getText().toString())) {
                    toast(R.string.finish);
                }
                break;
            case ConstantFitness.CALORIE:
                if ((int) calorie >= Integer.parseInt(tvTargetTraining.getText().toString())) {
                    toast(R.string.finish);
                }
                break;
        }
        runOnUiThread(() -> {
            textShowNumber.setText(String.valueOf(mRealCounts));
            textShowCalorie.setText(String.format(Locale.CHINA, "%.1f", calorie));
        });
    }

    private void showClock(TextView tv) {
        timer = new Timer();
        timer.schedule(new TrainTask(tv), 1000, 1000);
    }

    private void resetCalendar() {
        mCalendar.set(Calendar.HOUR_OF_DAY, 0);
        mCalendar.set(Calendar.MINUTE, 0);
        mCalendar.set(Calendar.SECOND, 0);
    }

    private void closeTimer() {
        if (timer != null) {
            timer.cancel();
            timer.purge();
            timer = null;
        }
    }

    class TrainTask extends TimerTask {
        TrainTask(TextView tv) {
            tvTargetShowtime = tv;
        }

        @Override
        public void run() {
            runOnUiThread(() -> {
                // 每次秒数+1
                mCalendar.add(Calendar.SECOND, 1);
                String s = DataCovertUtil.get2LenString(mCalendar.get(Calendar.MINUTE)) +
                        ":" + DataCovertUtil.get2LenString(mCalendar.get(Calendar.SECOND));
                tvTargetShowtime.setText(s);
            });
        }
    }
}
package com.abe.libfitness.main.program;

import android.view.View;
import android.widget.TextView;

import com.abe.libfitness.R;
import com.abe.libfitness.entity.ComReceiveDataBean;
import com.abe.libfitness.utils.DataCovertUtil;

import java.util.Calendar;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class AddMuscleActivity extends BaseProgramActivity {

    @Override
    protected void initProgram() {
        itemTitle.setText(R.string.AddMuscleActivity_title);
        tvNo1.setText(R.string.AddMuscleActivity_tv_no1);
        tvCount1.setText(R.string.AddMuscleActivity_tv_count1);
        tvInterTime1.setText(R.string.AddMuscleActivity_tv_intertime1);
        tvNo2.setText(R.string.AddMuscleActivity_tv_no2);
        tvCount2.setText(R.string.AddMuscleActivity_tv_count2);
        tvInterTime2.setText(R.string.AddMuscleActivity_tv_intertime1);
        tvNo3.setText(R.string.AddMuscleActivity_tv_no3);
        tvCount3.setText(R.string.AddMuscleActivity_tv_count3);
        tvInterTime3.setText(R.string.AddMuscleActivity_tv_intertime1);
        tvNo4.setText(R.string.AddMuscleActivity_tv_no4);
        tvCount4.setText(R.string.AddMuscleActivity_tv_count4);
        tvInterTime4.setText(R.string.AddMuscleActivity_tv_intertime1);
        tvNo5.setText(R.string.AddMuscleActivity_tv_no5);
        tvCount5.setText(R.string.AddMuscleActivity_tv_count5);
        mWeight = (int) (MAX_STRENGTH * 0.6);
    }

    @Override
    protected void onDataReceived(ComReceiveDataBean ComRecData) {
        if (ComRecData.verification()) {
            String res = DataCovertUtil.ByteArrToHex(ComRecData.bRec);
            final int control = DataCovertUtil.HexToInt(DataCovertUtil
                    .ByteArrToHex(ComRecData.bRec).substring(2, 4));
            if (control == 3) { // control指的返回的命令区分码
                // 实时值
                // final int weight = DataCovertUtil
                // .HexToInt(res.substring(6, 10));
                distance = DataCovertUtil.HexToInt(res.substring(10,
                        14));
                if (distance > 150) {
                    if (firstTime) {
                        resetCalendar();
                        showClock(mTimeTv);
                        firstTime = false;
                    }
                    resetTimer4LogOut();
                }

                if (distance >= MAX_DISTANCE * 0.8) { // 检测到距离大于60%最大距离时
                    mStatus = 1; // 拉伸状态变为1
                    if (distance4Calorie < distance) {
                        distance4Calorie = distance;
                    }
                }

                // 拉伸状为1并且 检测到距离小于30%最大距离时拉伸状态变为
                if (mStatus == 1 && distance < MAX_DISTANCE * 0.2) {
                    mRealCounts++; // 次数 + 1
                    mStatus = 0; // 拉伸状态清除

                    if (distance4Calorie >= 1500 && distance4Calorie < 2000) {
                        calorie = calorie + (mWeight / (180 + (Math.random() * 40)));
                    } else if (distance4Calorie >= 2000 && distance4Calorie < 3000) {
                        calorie = calorie + (mWeight / (130 + (Math.random() * 40)));
                    } else {
                        calorie = calorie + (mWeight / (80 + (Math.random() * 40)));
                    }
                }
            }

            if (mRealGroups == 1) {
                if (mRealCounts == 12) {
                    if (isCountDown) {
                        timer.cancel();
                        // 开启倒计时模式的计时器
                        mChronometer = new Timer();
                        mChronometer.schedule(new CountDown(mTimeTv), 1000, 1000);
                        isCountDown = false;
                    }
                }
            } else if (mRealGroups == 2) {
                if (mRealCounts == 10) {
                    if (isCountDown) {
                        timer.cancel();
                        // 开启倒计时模式的计时器
                        mChronometer = new Timer();
                        mChronometer.schedule(new CountDown(mTimeTv), 1000, 1000);
                        isCountDown = false;
                    }
                }
            } else if (mRealGroups == 3) {
                if (mRealCounts == 8) {
                    if (isCountDown) {
                        timer.cancel();
                        // 开启倒计时模式的计时器
                        mChronometer = new Timer();
                        mChronometer.schedule(new CountDown(mTimeTv), 1000, 1000);
                        isCountDown = false;
                    }
                }
            } else if (mRealGroups == 4) {
                if (mRealCounts == 6) {
                    if (isCountDown) {
                        timer.cancel();
                        // 开启倒计时模式的计时器
                        mChronometer = new Timer();
                        mChronometer.schedule(new CountDown(mTimeTv), 1000, 1000);
                        isCountDown = false;
                    }
                }
            } else {
                if (mRealCounts == 3) {
                    if (isCountDown) {
                        timer.cancel();
                        runOnUiThread(() -> {
                            mNo2Layout.setVisibility(View.GONE);
                            mNo3Layout.setVisibility(View.GONE);
                            mNo4Layout.setVisibility(View.GONE);
                            mNo5Layout.setVisibility(View.GONE);
                            mNo1Layout.setVisibility(View.GONE);
                            finish.setVisibility(View.VISIBLE);
                        });
                    }
                }
            }

            runOnUiThread(() -> {
                showTargetNumber.setText(String.valueOf(mRealCounts));
                mCalorieTv.setText(String.format(Locale.CHINA, "%.1f", calorie));
                mDialChartView.setPercent(distance * 100 / 3500, MAX_DISTANCE * 100 / 3500);
            });
        }
    }


    private class CountDown extends TimerTask {

        CountDown(TextView tv) {
            mCalendar.set(Calendar.SECOND, 60);
            mCalendar.set(Calendar.MINUTE, 0);
            mCalendar.set(Calendar.HOUR_OF_DAY, 0);
            mTimeTv = tv;
        }

        @Override
        public void run() {
            runOnUiThread(() -> {
                // 每次秒数-1
                mCalendar.add(Calendar.SECOND, -1);
                mTimeTv.setText(String.format("%s:%s", DataCovertUtil.get2LenString(mCalendar.get(Calendar.MINUTE)), DataCovertUtil.get2LenString(mCalendar.get(Calendar.SECOND))));
                if (mCalendar.get(Calendar.SECOND) == 0) {
                    mChronometer.cancel();
                    isCountDown = true;
                    showClock(mTimeTv);
                    mRealGroups++;
                    mShowRealGroups.setText(String.valueOf(mRealGroups));
                    mRealCounts = 0;
                    if (mRealGroups == 2) {
                        mWeight = (int) (MAX_STRENGTH * 0.7);
                        setTextSelectionBg(R.id.ll_no2);
                        closeSendThread();
                        // 新建发送任务
                        mSendThread = new SendThread(getForceCMD(mWeight), false);
                        mSendThread.start();
                    } else if (mRealGroups == 3) {
                        mWeight = (int) (MAX_STRENGTH * 0.8);
                        setTextSelectionBg(R.id.ll_no3);
                        closeSendThread();
                        // 新建发送任务
                        mSendThread = new SendThread(getForceCMD(mWeight), false);
                        mSendThread.start();
                    } else if (mRealGroups == 4) {
                        mWeight = (int) (MAX_STRENGTH * 0.9);
                        setTextSelectionBg(R.id.ll_no4);
                        closeSendThread();
                        // 新建发送任务
                        mSendThread = new SendThread(getForceCMD(mWeight), false);
                        mSendThread.start();
                    } else {
                        mWeight = (MAX_STRENGTH);
                        setTextSelectionBg(R.id.ll_no5);
                    }
                    mShowWeight.setText(String.valueOf(mWeight));
                }
            });
        }
    }
}
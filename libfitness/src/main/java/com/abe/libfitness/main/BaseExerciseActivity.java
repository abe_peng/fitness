package com.abe.libfitness.main;

import com.abe.libcore.utils.CommonUtils;
import com.abe.libfitness.base.BaseComActivity;
import com.abe.libfitness.entity.ComReceiveDataBean;
import com.abe.libfitness.entity.DataEntity;
import com.abe.libfitness.utils.ConstantFitness;
import com.abe.libfitness.utils.ProtocolV3Utils;

//基本运动（力量设置与读距离）
public abstract class BaseExerciseActivity extends BaseComActivity {
    //实际训练次数
    protected int mRealCounts = 0;
    //拉伸状态
    protected int mStatus;
    //记录单次拉伸的最大距离，计算卡路里
    protected int distance4Calorie = 0;
    //卡路里的值
    protected double calorie = 0;

    //是否正在设置力量
    protected boolean isWeightSetting;
    protected int mShowWeight = 10;//显示重量
    protected DataEntity dataEntity;

    protected abstract void initExercise();

    protected abstract void onDistance();

    @Override
    protected void initContent() {
        initExercise();
        weightSet(mShowWeight);
    }

    private int weightSetNum = 0;

    @Override
    protected void onDataReceived(ComReceiveDataBean ComRecData) {
        dataEntity = ProtocolV3Utils.parse(ComRecData.bRec);
        onDistance();
        if (dataEntity.getPower() != mShowWeight) {
            weightSetNum++;
            CommonUtils.INSTANCE.log("weightSetNum:"+weightSetNum);
            if (weightSetNum >= 5) {
                weightSet(mShowWeight);
                wBase("收到:" + json(ComRecData.bRec));
                weightSetNum = 0;
            }
        } else {
            weightSetNum = 0;
            wBase("收到:" + json(ComRecData.bRec));
        }
    }

    protected void weightSet(int weight) {
        isWeightSetting = true;
        builder.append("\n****************************力量:设置");
        builder.append(weight);
        builder.append("\n");
        builderW(ConstantFitness.LOG_FLAG_SEND);
        send(ProtocolV3Utils.writePower(weight));
    }
}
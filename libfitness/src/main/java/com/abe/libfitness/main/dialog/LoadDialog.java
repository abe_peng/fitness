package com.abe.libfitness.main.dialog;

import android.content.Context;

import com.abe.libfitness.R;
import com.abe.libfitness.base.BaseScreenDialog;

import org.jetbrains.annotations.NotNull;

public class LoadDialog extends BaseScreenDialog {
    public LoadDialog(@NotNull Context context) {
        super(context);
    }

    @Override
    protected Boolean isAutoZoom() {
        return true;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.item_dialog_loading;
    }

    @Override
    protected void initContent() {

    }
}

package com.abe.libfitness.main;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.abe.libfitness.R;
import com.abe.libfitness.R2;
import com.abe.libfitness.base.BaseActivity;
import com.abe.libfitness.utils.AppLanguageUtils;
import com.abe.libfitness.utils.ConstantFitness;
import com.abe.libfitness.utils.SpFitnessHelper;

import butterknife.BindView;
import butterknife.OnClick;

public class SettingActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R2.id.tv_csmm_img)
    ImageView tvCsmmImg;
    @BindView(R2.id.tv_dlfs_img)
    ImageView tvDlfsImg;
    @BindView(R2.id.tv_xzyy_img)
    ImageView tvXzyyImg;
    @BindView(R2.id.tv_wifi_img)
    ImageView tvWifiImg;
    @BindView(R2.id.ll_csmm)
    LinearLayout mLlCsmm;
    @BindView(R2.id.ll_dlfs)
    LinearLayout mLlDlfs;
    @BindView(R2.id.ll_xzyy)
    LinearLayout mLlXzyy;
    @BindView(R2.id.ll_wifi)
    LinearLayout mLlWifi;
    @BindView(R2.id.tv_csmm)
    TextView mTvCsmm;
    @BindView(R2.id.tv_dlfs)
    TextView mTvDlfs;
    @BindView(R2.id.tv_xzyy)
    TextView mTvXzyy;
    @BindView(R2.id.tv_wifi)
    TextView mTvWifi;
    @BindView(R2.id.ll_csmm_page)
    LinearLayout mCsmmPage;
    @BindView(R2.id.ll_dlfs_page)
    LinearLayout mDlfsPage;
    @BindView(R2.id.ll_xzyy_page)
    LinearLayout mXzyyPage;
    @BindView(R2.id.ll_wifi_page)
    LinearLayout mWifiPage;
    @BindView(R2.id.start_pwd)
    EditText mStartPwd;
    @BindView(R2.id.new_pwd)
    EditText mNewPwd;
    @BindView(R2.id.A)
    EditText mA;
    @BindView(R2.id.B)
    EditText mB;
    @BindView(R2.id.C)
    EditText mC;
    @BindView(R2.id.D)
    EditText mD;
    @BindView(R2.id.choose_china)
    TextView mXzyyChina;
    @BindView(R2.id.choose_english)
    TextView mXzyyEnglish;

    @BindView(R2.id.item_title)
    TextView itemTitle;
    @BindView(R2.id.set_card_login)
    ImageView mSetCardLogin;
    @BindView(R2.id.set_scan_login)
    ImageView mSetScanLogin;
    private Drawable ok;
    private int whiteColor, blackColor, suoColor, cardColor, scanColor, wifiColor;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_setting;
    }

    @Override
    protected void initContent() {
        ok = getResources().getDrawable(R.mipmap.ic_set_ok);
        ok.setBounds(0, 0, scale(35), scale(35));
        whiteColor = getResources().getColor(R.color.white_color);
        blackColor = getResources().getColor(R.color.black_color);
        suoColor = getResources().getColor(R.color.suo_color);
        cardColor = getResources().getColor(R.color.card_color);
        scanColor = getResources().getColor(R.color.scan_color);
        wifiColor = getResources().getColor(R.color.wifi_color);
        itemTitle.setText(R.string.setting_text);

        findViewById(R.id.wifi).setOnClickListener(this);
        findViewById(R.id.socket).setOnClickListener(this);
        findViewById(R.id.btn_ok_socket).setOnClickListener(this);
    }

    @OnClick(R2.id.iv_back)
    public void back() {
        gotoActivity(MainActivity.class);
        finish();
    }

    @OnClick({R2.id.tv_card, R2.id.tv_scan})
    public void loginModel(View view) {
        if (view.getId() == R.id.tv_scan) {
            if (mSetScanLogin.getVisibility() == View.VISIBLE) {
                mSetScanLogin.setVisibility(View.INVISIBLE);
                SpFitnessHelper.scanLogin(false);
            } else {
                mSetScanLogin.setVisibility(View.VISIBLE);
                SpFitnessHelper.scanLogin(true);
            }
        } else if (view.getId() == R.id.tv_card) {
            if (mSetCardLogin.getVisibility() == View.VISIBLE) {
                mSetCardLogin.setVisibility(View.INVISIBLE);
                SpFitnessHelper.cardLogin(false);
            } else {
                mSetCardLogin.setVisibility(View.VISIBLE);
                SpFitnessHelper.cardLogin(true);
            }
        }
    }

    @OnClick({R2.id.ll_csmm, R2.id.ll_dlfs, R2.id.ll_xzyy, R2.id.ll_wifi})
    public void menu(View view) {
        int id = view.getId();
        if (id == R.id.ll_csmm) {
            menuFormat(0);
        } else if (id == R.id.ll_dlfs) {
            menuFormat(1);
            mSetCardLogin.setVisibility(View.INVISIBLE);
            mSetScanLogin.setVisibility(View.INVISIBLE);
        } else if (id == R.id.ll_xzyy) {
            menuFormat(2);
            if (SpFitnessHelper.language().equals(ConstantFitness.LANGUAGE_ZH_CN)) {
                mXzyyChina.setCompoundDrawables(null, null, ok, null);
            } else {
                mXzyyEnglish.setCompoundDrawables(null, null, ok, null);
            }
        } else if (id == R.id.ll_wifi) {
            menuFormat(3);
            findViewById(R.id.ll_wifi_page_button).setVisibility(View.VISIBLE);
            findViewById(R.id.ll_wifi_page_socket).setVisibility(View.GONE);
        }
    }

    private void menuFormat(int pos) {
        LinearLayout[] layouts = {mLlCsmm, mLlDlfs, mLlXzyy, mLlWifi};
        ImageView[] imageViews = {tvCsmmImg, tvDlfsImg, tvXzyyImg, tvWifiImg};
        TextView[] textViews = {mTvCsmm, mTvDlfs, mTvXzyy, mTvWifi};
        LinearLayout[] pages = {mCsmmPage, mDlfsPage, mXzyyPage, mWifiPage};
        int[] color = {suoColor, cardColor, scanColor, wifiColor};
        int[] select = {R.mipmap.icon_setting_csmm_select, R.mipmap.icon_setting_dlfs_select,
                R.mipmap.icon_setting_xzyy_select, R.mipmap.icon_setting_wifi_select};
        int[] unSelect = {R.mipmap.icon_setting_csmm_not, R.mipmap.icon_setting_dlfs_not,
                R.mipmap.icon_setting_xzyy_not, R.mipmap.icon_setting_wifi_not};
        for (int i = 0; i < layouts.length; i++) {
            boolean status = pos == i;
            layouts[i].setBackgroundColor(status ? color[i] : blackColor);
            imageViews[i].setBackgroundResource(status ? select[i] : unSelect[i]);
            textViews[i].setTextColor(status ? whiteColor : color[i]);
            pages[i].setVisibility(status ? View.VISIBLE : View.GONE);
        }
    }

    @OnClick({R2.id.choose_china, R2.id.choose_english})
    public void language(View view) {
        int id = view.getId();
        if (id == R.id.choose_china) {
            if (SpFitnessHelper.language().equals(ConstantFitness.LANGUAGE_EN)) {
                mXzyyChina.setCompoundDrawables(null, null, ok, null);
                mXzyyEnglish.setCompoundDrawables(null, null, null, null);
                SpFitnessHelper.language(ConstantFitness.LANGUAGE_ZH_CN);
                AppLanguageUtils.language(this);
                reStart();
            }
        } else if (id == R.id.choose_english) {
            if (SpFitnessHelper.language().equals(ConstantFitness.LANGUAGE_ZH_CN)) {
                mXzyyEnglish.setCompoundDrawables(null, null, ok, null);
                mXzyyChina.setCompoundDrawables(null, null, null, null);
                SpFitnessHelper.language(ConstantFitness.LANGUAGE_EN);
                AppLanguageUtils.language(this);
                reStart();
            }
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btn_ok) {
            String iniPwd = mStartPwd.getText().toString();
            String newPwd = mNewPwd.getText().toString();
            if (TextUtils.isEmpty(iniPwd)) {
                toast(R.string.no_start_pwd);
            } else if (TextUtils.isEmpty(newPwd)) {
                toast(R.string.no_new_pwd);
            } else {
                if (iniPwd.equals(SpFitnessHelper.password())) {
                    toast(R.string.update_success);
                } else {
                    SpFitnessHelper.password(newPwd);
                    toast(R.string.update_fail);
                }
            }
        } else if (id == R.id.wifi) {
            startActivity(new Intent()
                    .setAction("android.net.wifi.PICK_WIFI_NETWORK"));
        } else if (id == R.id.socket) {
            findViewById(R.id.ll_wifi_page_button).setVisibility(View.GONE);
            findViewById(R.id.ll_wifi_page_socket).setVisibility(View.VISIBLE);
        } else if (id == R.id.btn_ok_socket) {
            String A = mA.getText().toString();
            String B = mB.getText().toString();
            String C = mC.getText().toString();
            String D = mD.getText().toString();
            if (Integer.parseInt(A) > 255 || Integer.parseInt(B) > 255
                    || Integer.parseInt(C) > 255 || Integer.parseInt(D) > 255) {
                Toast.makeText(this, "IP地址不合法", Toast.LENGTH_LONG).show();
                return;
            }
            String IP = A + "." + B + "." + C + "." + D;
            SpFitnessHelper.ip(IP);
            Toast.makeText(this, "IP地址设置成功", Toast.LENGTH_LONG).show();
        }
    }
}
package com.abe.libfitness.main;

import android.graphics.drawable.Drawable;
import android.widget.TextView;

import com.abe.libfitness.R;
import com.abe.libfitness.R2;
import com.abe.libfitness.base.BaseActivity;
import com.abe.libfitness.entity.TrainDataEntity;
import com.abe.libfitness.utils.SpFitnessHelper;
import com.abe.libquick.utils.NormalUtils;

import butterknife.BindView;
import butterknife.OnClick;

public class ReportActivity extends BaseActivity {
    @BindView(R2.id.item_title)
    TextView itemTitle;
    @BindView(R2.id.report_number)
    TextView reportNumber;
    @BindView(R2.id.report_number_l)
    TextView reportNumberL;
    @BindView(R2.id.report_time)
    TextView reportTime;
    @BindView(R2.id.report_time_l)
    TextView reportTimeL;
    @BindView(R2.id.report_calorie)
    TextView reportCalorie;
    @BindView(R2.id.report_calorie_l)
    TextView reportCalorieL;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_report;
    }

    @Override
    protected void initContent() {
        drawable(R.mipmap.icon_dlcs, reportNumberL);
        drawable(R.mipmap.icon_dlsj, reportTimeL);
        drawable(R.mipmap.icon_dlkll, reportCalorieL);
        TrainDataEntity dataEntity = SpFitnessHelper.train();
        reportNumber.setText(String.valueOf(dataEntity.getNumber()));
        reportTime.setText(String.valueOf(dataEntity.getTime()));
        reportCalorie.setText(NormalUtils.keepFormat(dataEntity.getCalorie(), 1));
    }

    private void drawable(int res, TextView textView) {
        Drawable img = getResources().getDrawable(res);
        img.setBounds(0, 0, 35, 35);
        textView.setCompoundDrawables(img, null, null, null);
    }

    @OnClick(R2.id.iv_back)
    public void back() {
        finish();
    }
}
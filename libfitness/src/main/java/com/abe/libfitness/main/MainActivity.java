package com.abe.libfitness.main;

import android.os.Bundle;

import com.abe.libfitness.R;
import com.abe.libfitness.R2;
import com.abe.libfitness.base.BaseActivity;
import com.abe.libfitness.main.program.ProgramActivity;
import com.abe.libfitness.utils.ConstantFitness;

import butterknife.OnClick;

public class MainActivity extends BaseActivity {

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_fitness_main_menu;
    }

    @Override
    protected void initContent() {
    }

    @OnClick(R2.id.iv_back)
    public void back() {
        finish();
    }

    @OnClick(R2.id.iv_sound_layout)
    public void sound() {
    }

    @OnClick(R2.id.ll_number)
    public void number() {
        Bundle bundle = new Bundle();
        bundle.putString(ConstantFitness.FLAG, ConstantFitness.NUMBER);
        gotoActivity(TotalActivity.class, bundle);
    }

    @OnClick(R2.id.ll_time)
    public void time() {
        Bundle bundle = new Bundle();
        bundle.putString(ConstantFitness.FLAG, ConstantFitness.TIME);
        gotoActivity(TotalActivity.class, bundle);
    }

    @OnClick(R2.id.ll_calorie)
    public void calorie() {
        Bundle bundle = new Bundle();
        bundle.putString(ConstantFitness.FLAG, ConstantFitness.CALORIE);
        gotoActivity(TotalActivity.class, bundle);
    }

    @OnClick(R2.id.ll_yy)
    public void language() {
        gotoActivity(PassActivity.class);
        finish();
    }

    @OnClick(R2.id.ll_test)
    public void test() {
        gotoActivity(DetectionActivity.class);
    }

    @OnClick(R2.id.ll_report)
    public void report() {
        gotoActivity(ReportActivity.class);
    }

    @OnClick(R2.id.ll_project)
    public void project() {
        gotoActivity(ProgramActivity.class);
    }
}
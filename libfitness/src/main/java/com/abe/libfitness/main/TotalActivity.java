package com.abe.libfitness.main;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.abe.libcore.base.screen.OnHandlerListener;
import com.abe.libfitness.R;
import com.abe.libfitness.R2;
import com.abe.libfitness.base.BaseActivity;
import com.abe.libfitness.utils.ConstantFitness;
import com.abe.libfitness.utils.SpFitnessHelper;
import com.abe.libquick.utils.quick.EmptyUtils;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.OnClick;

@SuppressLint("ClickableViewAccessibility")
public class TotalActivity extends BaseActivity implements SeekBar.OnSeekBarChangeListener, OnHandlerListener,
        View.OnTouchListener {
    @BindView(R2.id.tv_target_number)
    public TextView mTargetNumber;
    @BindView(R2.id.tv_target)
    public TextView mTargetValue;
    @BindView(R2.id.tv_target_unit)
    public TextView mUnit;
    @BindView(R2.id.sb_target)
    public SeekBar mSeekBar;
    protected int mTarget = 0;
    protected Timer timer;
    @BindView(R2.id.iv_target_reduce)
    ImageView ivTargetReduce;
    @BindView(R2.id.iv_target_plus)
    ImageView ivTargetPlus;
    private int max;
    private String flag;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_total;
    }

    @Override
    protected void initContent() {
        if (EmptyUtils.isEmpty(bundle()) || !bundle().containsKey(ConstantFitness.FLAG)) {
            finish();
            return;
        }
        flag = bundle().getString(ConstantFitness.FLAG);
        if (TextUtils.isEmpty(flag)) {
            finish();
            return;
        }
        switch (flag) {
            case ConstantFitness.NUMBER:
                max = ConstantFitness.MAX_NUMBER;
                mSeekBar.setMax(max);
                mTargetNumber.setText(R.string.target_number);
                mUnit.setText(R.string.number_uint);
                mSeekBar.setProgress(SpFitnessHelper.number());
                break;
            case ConstantFitness.TIME:
                max = ConstantFitness.MAX_TIME;
                mSeekBar.setMax(max);
                mTargetNumber.setText(R.string.target_time);
                mUnit.setText(R.string.time_uint);
                mSeekBar.setProgress(SpFitnessHelper.time());
                break;
            case ConstantFitness.CALORIE:
                max = ConstantFitness.MAX_CALORIE;
                mSeekBar.setMax(max);
                mTargetNumber.setText(R.string.target_calorie);
                mUnit.setText(R.string.calorie_uint);
                mSeekBar.setProgress(SpFitnessHelper.calorie());

                break;
        }
        mSeekBar.setOnSeekBarChangeListener(this);
        ivTargetReduce.setOnTouchListener(this);
        ivTargetPlus.setOnTouchListener(this);
    }

    @OnClick(R2.id.iv_back)
    public void back() {
        finish();
    }

    @OnClick(R2.id.iv_target_reduce)
    public void reduce() {
        if (mTarget > ConstantFitness.ZERO) {
            mTarget--;
            mTargetValue.setText(String.valueOf(mTarget));
            mSeekBar.setProgress(mTarget);
        }
    }

    @OnClick(R2.id.iv_target_plus)
    public void plus() {
        if (mTarget < max) {
            mTarget++;
            mTargetValue.setText(String.valueOf(mTarget));
            mSeekBar.setProgress(mTarget);
        }
    }

    @OnClick(R2.id.ll_go)
    public void go() {
        Bundle bundle = new Bundle();
        bundle.putInt(ConstantFitness.TARGET, mTarget);
        bundle.putString(ConstantFitness.FLAG, flag);
        switch (flag) {
            case ConstantFitness.NUMBER:
                SpFitnessHelper.number(mTarget);
                break;
            case ConstantFitness.TIME:
                SpFitnessHelper.time(mTarget);
                break;
            case ConstantFitness.CALORIE:
                SpFitnessHelper.calorie(mTarget);
                break;
        }
        gotoActivity(TrainingActivity.class, bundle);
        finish();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        mTarget = progress;
        mTargetValue.setText(String.valueOf(progress));
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                // 只要当down返回true时候，系统将不把本次事件记录为点击事件，也就不会触发onClick或者onLongClick事件了
                timer = new Timer(true);
                if (v.getId() == R.id.iv_target_reduce) {
                    timer.schedule(new TimeTask(ConstantFitness.ACTION_REDUCE), 1000, 100);
                } else {
                    timer.schedule(new TimeTask(ConstantFitness.ACTION_PLUS), 1000, 100);
                }
                break;

            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                timer.cancel();
                timer.purge();
                timer = null;
                break;
        }
        return false;
    }

    @Override
    public void dispatchHandleMessage(Message msg) {
        switch (msg.what) {
            case ConstantFitness.ACTION_REDUCE:
                if (mTarget > ConstantFitness.ZERO) {
                    mTarget--;
                    mTargetValue.setText(String.valueOf(mTarget));
                    mSeekBar.setProgress(mTarget);
                }
                break;
            case ConstantFitness.ACTION_PLUS:
                if (mTarget < max) {
                    mTarget++;
                    mTargetValue.setText(String.valueOf(mTarget));
                    mSeekBar.setProgress(mTarget);
                }
                break;
        }
    }

    public class TimeTask extends TimerTask {
        private int type;

        TimeTask(int type) {
            this.type = type;
        }

        @Override
        public void run() {
            localEmptyMsg(type);
        }
    }
}
package com.abe.libfitness.main;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.abe.libcore.base.permission.PermissionUtils;
import com.abe.libfitness.R;
import com.abe.libfitness.R2;
import com.abe.libfitness.base.BaseActivity;
import com.abe.libfitness.main.exercise.ExerciseActivity;
import com.abe.libfitness.utils.ConstantFitness;
import com.yanzhenjie.permission.runtime.Permission;

import butterknife.OnClick;

public class InitSettingV2Activity extends BaseActivity {
    private boolean isGotoMain;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_fitness_init;
    }

    @Override
    protected void initContent() {
        isGotoMain = false;
        refresh();
        //gotoActivity(TestActivity.class);
    }

    @OnClick(R2.id.item_btn_refresh)
    public void refresh() {
        PermissionUtils.INSTANCE.request(this, () -> {
            Bundle bundle = new Bundle();
            bundle.putBoolean(ConstantFitness.START_MOTOR, true);
            gotoActivityForResult(MotorStartStopActivity.class, bundle, ConstantFitness.REQUEST_MOTOR_ACTION);
        }, Permission.Group.STORAGE);
    }


    private synchronized void gotoMain() {
        if (isGotoMain) return;
        isGotoMain = true;
        hideLoadingView();
        gotoActivity(ExerciseActivity.class);
        finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) return;
        if (ConstantFitness.REQUEST_MOTOR_ACTION == requestCode) {
            gotoMain();
        }
    }
}
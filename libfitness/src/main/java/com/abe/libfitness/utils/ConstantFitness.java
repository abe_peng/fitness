package com.abe.libfitness.utils;

import com.abe.libcore.ConstantCore;

import java.io.File;

public class ConstantFitness {
    public static final String FILE_ROOT_PATH = ConstantCore.INSTANCE.getFILE_ROOT_PATH() + File.separator + "fitness";
    public static final String CORE_PATH = FILE_ROOT_PATH + File.separator + "core";
    public static final String LOG_PATH = FILE_ROOT_PATH + File.separator + "log";
    public static final String LOG_HTTP_PATH = FILE_ROOT_PATH + File.separator + "http";
    public static final String LOG_SERIAL_PATH = FILE_ROOT_PATH + File.separator + "serialPort";

    ////////////////////////////////////////////////////////////////////////////////////////////////

    public static final String LANGUAGE_ZH_CN = "zh-CN";
    public static final String LANGUAGE_EN = "en";
    public static final String FLAG = "flag";
    public static final String NUMBER = "Number";
    public static final String TIME = "Time";
    public static final String CALORIE = "Calorie";
    public static final String TARGET = "Target";
    public static final String START_MOTOR = "startMotor";
    public static final String LOG_FLAG_TEST = "Test";
    public static final String LOG_FLAG_DATA = "Data";
    public static final String LOG_FLAG_SEND = "Send";

    public static final int ZERO = 0;
    public static final int MAX_NUMBER = 100;
    public static final int MAX_TIME = 60;
    public static final int MAX_CALORIE = 200;

    public static final int ACTION_REDUCE = 0x001;
    public static final int ACTION_PLUS = 0x002;

    public static final int REQUEST_MOTOR_ACTION = 0x101;
}

package com.abe.libfitness.utils;

public class HtmlHelper {
    public static final String BASE_URL = "file:///android_asset/fitness/";
    public static final String FITNESS = BASE_URL + "chart_fitness.html";
    public static final String BALL = BASE_URL + "chart_ball.html";
}
package com.abe.libfitness.utils;

import java.util.Arrays;

/**
 * 数据转换工具
 */
public class DataCovertUtil {
    /** 判断奇数或偶数，位运算，最后一位是1则为奇数，为0是偶数 */
    static public int isOdd(int num) {
        return num & 0x1;
    }

    /** Hex字符串转int */
    static public int HexToInt(String inHex) {
        return Integer.parseInt(inHex, 16);
    }

    /** Hex字符串转byte */
    static public byte HexToByte(String inHex) {
        return (byte) Integer.parseInt(inHex, 16);
    }

    /** 1字节转2个Hex字符 */
    static public String Byte2Hex(Byte inByte) {
        return String.format("%02x", inByte).toUpperCase();
    }

    /** 字节数组转转hex字符串 */
    static public String ByteArrToHex(byte[] inBytArr) {
        StringBuilder strBuilder = new StringBuilder();
        int j = inBytArr.length;
        for (int i = 0; i < j; i++) {
            strBuilder.append(Byte2Hex(inBytArr[i]));
            // strBuilder.append(" ");
        }
        return strBuilder.toString();
    }

    /** 字节数组转转hex字符串，可选长度 */
    static public String ByteArrToHex(byte[] inBytArr, int offset, int byteCount) {
        StringBuilder strBuilder = new StringBuilder();
        int j = byteCount;
        for (int i = offset; i < j; i++) {
            strBuilder.append(Byte2Hex(inBytArr[i]));
        }
        return strBuilder.toString();
    }

    /** 转hex字符串转字节数组 */
    static public byte[] HexToByteArr(String inHex) {
        int hexlen = inHex.length();
        byte[] result;
        if (isOdd(hexlen) == 1) {// 奇数
            hexlen++;
            result = new byte[(hexlen / 2)];
            inHex = "0" + inHex;
        } else {// 偶数
            result = new byte[(hexlen / 2)];
        }
        int j = 0;
        for (int i = 0; i < hexlen; i += 2) {
            result[j] = HexToByte(inHex.substring(i, i + 2));
            j++;
        }
        return result;
    }

    /** 计算异或 */
    static public String ComputeXOR(byte[] cmd) {
        byte A = 0;
        for (int i = 0; i < cmd.length; i++) {
            A ^= cmd[i];
        }
        return String.format("%02x", A).toUpperCase();
    }

//    /** 获得指纹识别完整命令 */
//    static public byte[] getFingerCMD(byte[] cmd) {
//        StringBuilder strBuilder = new StringBuilder();
//        strBuilder.append("F5");
//        strBuilder.append(ByteArrToHex(cmd).toUpperCase());
//        strBuilder.append(ComputeXOR(cmd));
//        strBuilder.append("F5");
//        return HexToByteArr(strBuilder.toString());
//    }


    /**
     * 获得带有CRC-16 Modbus模式校验码的命令
     * 
     * @param bytes
     *            需要添加CRC校验码的命令
     * @return 添加CRC校验码完成的命令
     */
    public static byte[] getCRC(byte[] bytes) {
        int CRC = 0x0000ffff;
        int POLYNOMIAL = 0x0000a001;
        int i, j;
        byte[] cmd = new byte[bytes.length + 2];
        String crcString = "";
        for (i = 0; i < bytes.length; i++) {
            CRC ^= ((int) bytes[i] & 0x000000ff);
            for (j = 0; j < 8; j++) {
                if ((CRC & 0x00000001) != 0) {
                    CRC >>= 1;
                    CRC ^= POLYNOMIAL;
                } else {
                    CRC >>= 1;
                }
            }
            cmd[i] = bytes[i];
        }
        crcString = get4LenString(Integer.toHexString(CRC));
        cmd[i++] = HexToByte(crcString.substring(2, 4));
        cmd[i] = HexToByte(crcString.substring(0, 2));
        return cmd;
    }

    /**
     * 校验命令返回
     * 
     * @param bCmd
     *            发送的命令
     * @param bRec
     *            返回的命令
     * @return
     */
    public static boolean checkCMD(byte[] bCmd, byte[] bRec) {
        String cmd = ByteArrToHex(bCmd);
        String res = ByteArrToHex(bRec);
        // 写命令
        if ("06".equals(cmd.substring(2, 4))) {
            if (cmd.equals(res)) {
                return true;
            } else {
                return false;
            }
        } else {
            String cmdPart = res.substring(0, res.length() - 4);
            if (Arrays.equals(bRec, getCRC(HexToByteArr((cmdPart))))) {
                return true;
            } else {
                return false;
            }
        }
    }

    public static String get4LenString(String sting) {
        StringBuilder stringBuilder = new StringBuilder();
        switch (sting.length()) {
            case 1:
                stringBuilder.append("000");
                break;
            case 2:
                stringBuilder.append("00");
                break;
            case 3:
                stringBuilder.append("0");
                break;
            default:
                break;
        }
        stringBuilder.append(sting);
        return stringBuilder.toString();
    }

    public static String get2LenString(int num) {
        String string = Integer.toString(num);
        StringBuilder stringBuilder = new StringBuilder();
        switch (string.length()) {
            case 1:
                stringBuilder.append("0");
                break;
            default:
                break;
        }
        stringBuilder.append(string);
        return stringBuilder.toString();
    }

}
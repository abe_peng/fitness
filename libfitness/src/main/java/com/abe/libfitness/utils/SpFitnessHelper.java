package com.abe.libfitness.utils;

import android.text.TextUtils;

import com.abe.libcore.utils.SPUtils;
import com.abe.libcore.utils.context.ContextUtils;
import com.abe.libfitness.entity.TrainDataEntity;
import com.abe.libquick.api.JsonHelper;

public class SpFitnessHelper {
    private static final String SP_BASE_TAG = "com.abe.libfitness";
    private static final String SP_PASSWORD = "password";
    private static final String SP_TargetNumber = "TargetNumber";
    private static final String SP_TargetTime = "TargetTime";
    private static final String SP_TargetCalorie = "TargetCalorie";
    private static final String SP_TRAIN_DATA = "trainData";
    private static final String SP_LANGUAGE_KEY = "language";
    private static final String SP_EXPERIENCE_MODE = "experienceMode";
    private static final String SP_LOGIN_CARD = "cardLogin";
    private static final String SP_LOGIN_SCAN = "scanLogin";
    private static final String SP_IP = "ip";
    private static final String SP_MAX_STRENGTH = "strength";
    private static final String SP_MAX_DISTANCE = "distance";
    private static final String SP_IS_PORTRAIT = "isPortrait";

    private static SPUtils spUtils;

    protected static SPUtils sp() {
        if (spUtils == null) {
            spUtils = new SPUtils(ContextUtils.Companion.getContext(), SP_BASE_TAG);
        }
        return spUtils;
    }

    public static void password(String password) {
        sp().putString(SP_PASSWORD, password);
    }

    public static String password() {
        return sp().getString(SP_PASSWORD, "123456");
    }

    public static void number(int number) {
        sp().putInt(SP_TargetNumber, number);
    }

    public static int number() {
        return sp().getInt(SP_TargetNumber, 0);
    }

    public static void time(int time) {
        sp().putInt(SP_TargetTime, time);
    }

    public static int time() {
        return sp().getInt(SP_TargetTime, 0);
    }

    public static void calorie(int calorie) {
        sp().putInt(SP_TargetCalorie, calorie);
    }

    public static int calorie() {
        return sp().getInt(SP_TargetCalorie, 0);
    }

    public static void train(TrainDataEntity entity) {
        sp().putString(SP_TRAIN_DATA, JsonHelper.gson(JsonHelper.JSON_NORMAL).toJson(entity));
    }

    public static TrainDataEntity train() {
        String json = sp().getString(SP_TargetCalorie, "");
        json = TextUtils.isEmpty(json) ? JsonHelper.gson(JsonHelper.JSON_NORMAL).toJson(new TrainDataEntity()) : json;
        return JsonHelper.gson(JsonHelper.JSON_NORMAL).fromJson(json, TrainDataEntity.class);
    }

    public static void language(String language) {
        sp().putString(SP_LANGUAGE_KEY, language);
    }

    public static String language() {
        return sp().getString(SP_LANGUAGE_KEY, ConstantFitness.LANGUAGE_ZH_CN);
    }

    public static void experience(boolean experience) {
        sp().putBoolean(SP_EXPERIENCE_MODE, experience);
    }

    public static boolean experience() {
        return sp().getBoolean(SP_EXPERIENCE_MODE, false);
    }

    public static void cardLogin(boolean cardLogin) {
        sp().putBoolean(SP_LOGIN_CARD, cardLogin);
    }

    public static boolean cardLogin() {
        return sp().getBoolean(SP_LOGIN_CARD, false);
    }

    public static void scanLogin(boolean scanLogin) {
        sp().putBoolean(SP_LOGIN_SCAN, scanLogin);
    }

    public static boolean scanLogin() {
        return sp().getBoolean(SP_LOGIN_SCAN, false);
    }


    public static void ip(String ip) {
        sp().putString(SP_IP, ip);
    }

    public static String ip() {
        return sp().getString(SP_IP, "");
    }

    public static void strength(int strength) {
        sp().putInt(SP_MAX_STRENGTH, strength);
    }

    public static int strength() {
        return sp().getInt(SP_MAX_STRENGTH, 0);
    }

    public static void distance(int distance) {
        sp().putInt(SP_MAX_DISTANCE, distance);
    }

    public static int distance() {
        return sp().getInt(SP_MAX_DISTANCE, 0);
    }

    public static void screen(boolean isPortrait) {
        sp().putBoolean(SP_IS_PORTRAIT, isPortrait);
    }

    public static boolean isPortrait() {
        return sp().getBoolean(SP_IS_PORTRAIT, true);
    }
}
package com.abe.libfitness.utils;

public class CMDConts {

    public static int[] STATUS = {1, 0};
    public static int INIT_CMD_CONUNT = 3;

    public static byte[] CMD_READ_REGISTER_2 = {(byte) 0x01, (byte) 0x03, (byte) 0x00, (byte) 0x02, (byte) 0x00, (byte) 0x01, (byte) 0x25, (byte) 0xCA};

    public static byte[] CMD_READ_REGISTER_3_4 = {(byte) 0x01, (byte) 0x03, (byte) 0x00, (byte) 0x03, (byte) 0x00, (byte) 0x02, (byte) 0x34, (byte) 0x0B};
    //main页发送
    public static byte[] CMD_WRITE_REGISTER_7 = {(byte) 0x01, (byte) 0x06, (byte) 0x00, (byte) 0x07, (byte) 0x00, (byte) 0x01, (byte) 0xF9, (byte) 0xCB};
    //距离检测（写）
    public static byte[] CMD_WRITE_D_REIGSTER_F0 = {(byte) 0x01, (byte) 0x06, (byte) 0x00, (byte) 0x08, (byte) 0x01, (byte) 0x2C, (byte) 0x08, (byte) 0x45};
    public static byte[] CMD_WRITE_D_REIGSTER_F1 = {(byte) 0x01, (byte) 0x06, (byte) 0x00, (byte) 0x09, (byte) 0x01, (byte) 0xF4, (byte) 0x59, (byte) 0xDF};
    public static byte[] CMD_WRITE_D_REIGSTER_F2 = {(byte) 0x01, (byte) 0x06, (byte) 0x00, (byte) 0x0A, (byte) 0x01, (byte) 0xF4, (byte) 0xA9, (byte) 0xDF};
    public static byte[] CMD_WRITE_D_REIGSTER_F3 = {(byte) 0x01, (byte) 0x06, (byte) 0x00, (byte) 0x0B, (byte) 0x01, (byte) 0xF4, (byte) 0xF8, (byte) 0x1F};
    public static byte[] CMD_WRITE_D_REIGSTER_F4 = {(byte) 0x01, (byte) 0x06, (byte) 0x00, (byte) 0x0C, (byte) 0x01, (byte) 0xF4, (byte) 0x49, (byte) 0xDE};
    public static byte[] CMD_WRITE_D_REIGSTER_F5 = {(byte) 0x01, (byte) 0x06, (byte) 0x00, (byte) 0x0D, (byte) 0x01, (byte) 0xF4, (byte) 0x18, (byte) 0x1E};
    //距离检测（读）
    public static byte[] CMD_READ_REGISTER_4 = {(byte) 0x01, (byte) 0x03, (byte) 0x00, (byte) 0x04, (byte) 0x00, (byte) 0x01, (byte) 0xC5, (byte) 0xCB};
    //肌力测试（写）
    public static byte[] CMD_WRITE_F_REIGSTER_F0 = {(byte) 0x01, (byte) 0x06, (byte) 0x00, (byte) 0x08, (byte) 0x01, (byte) 0x2C, (byte) 0x08, (byte) 0x45};
    public static byte[] CMD_WRITE_F_REIGSTER_F1 = {(byte) 0x01, (byte) 0x06, (byte) 0x00, (byte) 0x09, (byte) 0x03, (byte) 0xE8, (byte) 0x59, (byte) 0x76};
    public static byte[] CMD_WRITE_F_REIGSTER_F2 = {(byte) 0x01, (byte) 0x06, (byte) 0x00, (byte) 0x0A, (byte) 0x07, (byte) 0xD0, (byte) 0xAA, (byte) 0x64};
    public static byte[] CMD_WRITE_F_REIGSTER_F3 = {(byte) 0x01, (byte) 0x06, (byte) 0x00, (byte) 0x0B, (byte) 0x0B, (byte) 0xB8, (byte) 0xFF, (byte) 0x4A};
    public static byte[] CMD_WRITE_F_REIGSTER_F4 = {(byte) 0x01, (byte) 0x06, (byte) 0x00, (byte) 0x0C, (byte) 0x0E, (byte) 0x53, (byte) 0x0D, (byte) 0x94};
    public static byte[] CMD_WRITE_F_REIGSTER_F5 = {(byte) 0x01, (byte) 0x06, (byte) 0x00, (byte) 0x0D, (byte) 0x0E, (byte) 0x53, (byte) 0x5C, (byte) 0x54};
    public static byte[] CMD_WRITE_F_REIGSTER_D0 = {(byte) 0x01, (byte) 0x06, (byte) 0x00, (byte) 0x0E, (byte) 0x00, (byte) 0x32, (byte) 0x69, (byte) 0xDC};
    public static byte[] CMD_WRITE_F_REIGSTER_D1 = {(byte) 0x01, (byte) 0x06, (byte) 0x00, (byte) 0x0F, (byte) 0x00, (byte) 0xC8, (byte) 0xB8, (byte) 0x5F};
    public static byte[] CMD_WRITE_F_REIGSTER_D2 = {(byte) 0x01, (byte) 0x06, (byte) 0x00, (byte) 0x10, (byte) 0x01, (byte) 0x90, (byte) 0x89, (byte) 0xF3};
    public static byte[] CMD_WRITE_F_REIGSTER_D3 = {(byte) 0x01, (byte) 0x06, (byte) 0x00, (byte) 0x11, (byte) 0x02, (byte) 0x58, (byte) 0xD9, (byte) 0x55};
    public static byte[] CMD_WRITE_F_REIGSTER_D4 = {(byte) 0x01, (byte) 0x06, (byte) 0x00, (byte) 0x12, (byte) 0x03, (byte) 0xE8, (byte) 0x29, (byte) 0x71};
    public static byte[] CMD_WRITE_F_REIGSTER_D5 = {(byte) 0x01, (byte) 0x06, (byte) 0x00, (byte) 0x13, (byte) 0x07, (byte) 0xD0, (byte) 0x7B, (byte) 0xA3};
    //肌力测试（读）
    public static byte[] CMD_READ_REGISTER_3 = {(byte) 0x01, (byte) 0x03, (byte) 0x00, (byte) 0x03, (byte) 0x00, (byte) 0x02, (byte) 0x34, (byte) 0x0B};//修改

    //力量命令组
    public static byte[] CMD_WRITE_REIGSTER_F0 = {(byte) 0x01, (byte) 0x06, (byte) 0x00, (byte) 0x08, (byte) 0x01, (byte) 0x2C, (byte) 0x08, (byte) 0x45};
    public static String CMD_WRITE_REIGSTER_F1 = "01060009";
    public static String CMD_WRITE_REIGSTER_F2 = "0106000A";
    public static String CMD_WRITE_REIGSTER_F3 = "0106000B";
    public static String CMD_WRITE_REIGSTER_F4 = "0106000C";
    public static String CMD_WRITE_REIGSTER_F5 = "0106000D";
    public static byte[] CMD_WRITE_REIGSTER_D0 = {(byte) 0x01, (byte) 0x06, (byte) 0x00, (byte) 0x0E, (byte) 0x00, (byte) 0x0A, (byte) 0x68, (byte) 0x0E};
    public static byte[] CMD_WRITE_REIGSTER_D1 = {(byte) 0x01, (byte) 0x06, (byte) 0x00, (byte) 0x0F, (byte) 0x00, (byte) 0x50, (byte) 0xB9, (byte) 0xF5};
    public static String CMD_WRITE_REIGSTER_D2 = "01060010";
    public static String CMD_WRITE_REIGSTER_D3 = "01060011";
    public static String CMD_WRITE_REIGSTER_D4 = "01060012";
    public static String CMD_WRITE_REIGSTER_D5 = "01060013";
}
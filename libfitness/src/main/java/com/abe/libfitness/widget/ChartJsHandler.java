package com.abe.libfitness.widget;

import android.content.Context;
import android.webkit.JavascriptInterface;

import com.abe.libcore.utils.CommonUtils;

public class ChartJsHandler {
    private Context context;

    public ChartJsHandler(Context context) {
        this.context = context;
    }

    @JavascriptInterface
    public void chartJsCall(String json) {
        CommonUtils.INSTANCE.log(json);
    }
}

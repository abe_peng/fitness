package com.abe.libfitness.entity;

public class ExerciseEntity {
    public int weight;
    public float distance;

    public ExerciseEntity() {
    }

    public ExerciseEntity(int weight, float distance) {
        this.weight = weight;
        this.distance = distance;
    }
}
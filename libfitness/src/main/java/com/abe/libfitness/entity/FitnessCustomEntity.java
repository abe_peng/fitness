package com.abe.libfitness.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class FitnessCustomEntity implements Parcelable {
    public static final Creator<FitnessCustomEntity> CREATOR = new Creator<FitnessCustomEntity>() {
        @Override
        public FitnessCustomEntity createFromParcel(Parcel in) {
            return new FitnessCustomEntity(in);
        }

        @Override
        public FitnessCustomEntity[] newArray(int size) {
            return new FitnessCustomEntity[size];
        }
    };
    private int weight01;
    private int weight02;
    private int weight03;

    public FitnessCustomEntity() {
    }

    public FitnessCustomEntity(int weight01, int weight02, int weight03) {
        this.weight01 = weight01;
        this.weight02 = weight02;
        this.weight03 = weight03;
    }

    protected FitnessCustomEntity(Parcel in) {
        weight01 = in.readInt();
        weight02 = in.readInt();
        weight03 = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(weight01);
        dest.writeInt(weight02);
        dest.writeInt(weight03);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public int getWeight01() {
        return weight01;
    }

    public void setWeight01(int weight01) {
        this.weight01 = weight01;
    }

    public int getWeight02() {
        return weight02;
    }

    public void setWeight02(int weight02) {
        this.weight02 = weight02;
    }

    public int getWeight03() {
        return weight03;
    }

    public void setWeight03(int weight03) {
        this.weight03 = weight03;
    }
}

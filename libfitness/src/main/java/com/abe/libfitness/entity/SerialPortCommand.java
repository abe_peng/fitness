package com.abe.libfitness.entity;

import android.text.TextUtils;

import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

public class SerialPortCommand {
    public static final String COM_TYPE_EM_START = "em-start";//电机启动
    public static final String COM_TYPE_EM_STOP = "em-stop";//电机停止
    public static final String COM_TYPE_EM_STATUS = "em-status";//读电机状态
    public static final String COM_TYPE_SET_POWER = "set-power";//设置力量
    public static final String COM_TYPE_READ_POWER = "read-power";//读力量
    public static final String COM_TYPE_READ_DISTANCE = "distance";//距离读取
    private static final String COM_TYPE_NORMAL = "normal";//普通
    //命令
    public byte[] command;
    //命令类型
    public String type;
    //命令唯一标识（时间戳）
    private Long serialId;

    public SerialPortCommand(Long serialId, byte[] command, String type) {
        this.serialId = serialId;
        this.command = command;
        this.type = type;
    }

    public SerialPortCommand(Long serialId, byte[] command) {
        this(serialId, command, COM_TYPE_NORMAL);
    }

    public SerialPortCommand(byte[] command, String type) {
        this(System.currentTimeMillis(), command, type);
    }

    public SerialPortCommand(byte[] command) {
        this(command, COM_TYPE_NORMAL);
    }

    public SerialPortCommand() {
    }

    public Long getSerialId() {
        return serialId;
    }

    @NotNull
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("命令类型：")
                .append(TextUtils.isEmpty(type) ? COM_TYPE_NORMAL : type)
                .append("--->>>命令：")
                .append(command != null ? new Gson().toJson(command) : "null");
        return builder.toString();
    }
}
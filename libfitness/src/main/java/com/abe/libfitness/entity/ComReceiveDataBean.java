package com.abe.libfitness.entity;

import com.abe.libfitness.utils.ProtocolV3Utils;

public class ComReceiveDataBean {
    public byte[] bRec;

    public ComReceiveDataBean(byte[] buffer, int size) {
        bRec = new byte[size];
        System.arraycopy(buffer, 0, bRec, 0, size);
    }

    public boolean verification() {
        return ProtocolV3Utils.verification(bRec);
    }

    public boolean oldVerification() {
        return getCRC(bRec).equals("0");
    }

    private String getCRC(byte[] bytes) {
        int CRC = 0x0000ffff;
        int POLYNOMIAL = 0x0000a001;
        int i, j;
        for (i = 0; i < bytes.length; i++) {
            CRC ^= ((int) bytes[i] & 0x000000ff);
            for (j = 0; j < 8; j++) {
                if ((CRC & 0x00000001) != 0) {
                    CRC >>= 1;
                    CRC ^= POLYNOMIAL;
                } else {
                    CRC >>= 1;
                }
            }
        }
        return Integer.toHexString(CRC);
    }
}
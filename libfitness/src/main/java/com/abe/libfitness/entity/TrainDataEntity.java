package com.abe.libfitness.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class TrainDataEntity implements Parcelable {
    public static final Creator<TrainDataEntity> CREATOR = new Creator<TrainDataEntity>() {
        @Override
        public TrainDataEntity createFromParcel(Parcel in) {
            return new TrainDataEntity(in);
        }

        @Override
        public TrainDataEntity[] newArray(int size) {
            return new TrainDataEntity[size];
        }
    };
    private int number;
    private int time;
    private double calorie;

    public TrainDataEntity() {
        this.number = 0;
        this.time = 0;
        this.calorie = 0;
    }

    protected TrainDataEntity(Parcel in) {
        number = in.readInt();
        time = in.readInt();
        calorie = in.readFloat();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(number);
        dest.writeInt(time);
        dest.writeDouble(calorie);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public double getCalorie() {
        return calorie;
    }

    public void setCalorie(double calorie) {
        this.calorie = calorie;
    }
}

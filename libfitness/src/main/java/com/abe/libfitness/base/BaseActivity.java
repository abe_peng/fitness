package com.abe.libfitness.base;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;

import com.abe.libcore.base.screen.j.AbeJActivity;
import com.abe.libcore.utils.CommonUtils;
import com.abe.libfitness.main.dialog.LoadDialog;
import com.abe.libfitness.utils.AppLanguageUtils;
import com.google.gson.Gson;

import butterknife.ButterKnife;

public abstract class BaseActivity extends AbeJActivity {
    protected static final int loading = 0x120;
    protected boolean isAlive = false;
    protected String TAG = getClass().getSimpleName();
    private LoadDialog dialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isAlive = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isAlive = false;
    }

    /**
     * 如果是7.0以下，我们需要调用changeAppLanguage方法，
     * 如果是7.0及以上系统，直接把我们想要切换的语言类型保存在SharedPreferences中,然后重新启动MainActivity即可
     */
    public void reStart() {
        Intent intent = new Intent(this, this.getClass());
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(AppLanguageUtils.attachBaseContext(newBase));
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        ButterKnife.bind(this);
    }

    @Override
    public Boolean isAutoZoom() {
        return true;
    }

    public Bundle bundle() {
        return getIntent().getExtras();
    }

    public void localEmptyMsg(int what) {
        getHandler().sendEmptyMessage(what);
    }

    public void toast(int strRes) {
        toast(getResources().getString(strRes));
    }

    protected void showLoadingView() {
        showDialog(loading);
    }

    protected void hideLoadingView() {
        if (dialog != null && dialog.isShowing())
            dismissDialog(loading);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        if (id == loading && dialog == null) {
            dialog = new LoadDialog(this);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            dialog.show();
        }
        return dialog;
    }

    //获取缩放比例
    protected Float getScale() {
        int designWidth = (int) CommonUtils.INSTANCE.getMetaData(this, "designWidth", 0);
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        return ((float) width / (float) designWidth);
    }

    protected int scale(float value) {
        return (int) (getScale() * value);
    }

    protected String json(Object obj) {
        return new Gson().toJson(obj);
    }

    protected void wBase(String message) {
        System.out.println("动作状态：--------------------" + message);
    }
}
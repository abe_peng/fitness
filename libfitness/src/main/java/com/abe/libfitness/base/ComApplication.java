package com.abe.libfitness.base;

import android.content.Context;
import android.util.Log;

import com.abe.libcore.utils.CommonUtils;
import com.abe.libcore.utils.context.ContextUtils;
import com.abe.libfitness.utils.AppLanguageUtils;
import com.abe.libquick.base.BaseApplication;
import com.tencent.smtt.export.external.TbsCoreSettings;
import com.tencent.smtt.sdk.QbSdk;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import android_serialport_api.SerialPort;

public class ComApplication extends BaseApplication {

    // public static String mPortPath = "/dev/ttyS0";
    //public static String mPortPath = "/dev/ttyS2";
    public static String mPortPath = "/dev/ttyS3";
    //波特率
    public static int mBaudRate = 19200;
    private static ComApplication application;
    private String TAG = getClass().getSimpleName();
    private SerialPort mSerialPort = null;

    public static ComApplication Application() {
        return application;
    }

    @Override
    protected void attachBaseContext(Context base) {
        ContextUtils.Companion.init(base);
        super.attachBaseContext(AppLanguageUtils.attachBaseContext(base));
    }

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
        initX5();
    }

    //获得串口对象
    public SerialPort getSerialPortInstance() {
        if (mSerialPort == null) {
            try {
                mSerialPort = new SerialPort(new File(mPortPath), mBaudRate, 0);
            } catch (SecurityException | IOException e) {
                e.printStackTrace();
                Log.d(TAG, "getSerialPortInstance:" + e.toString());
            }
        }
        return mSerialPort;
    }

    private void initX5() {
        // 在调用TBS初始化、创建WebView之前进行如下配置
        HashMap<String, Object> map = new HashMap<>();
        map.put(TbsCoreSettings.TBS_SETTINGS_USE_SPEEDY_CLASSLOADER, true);
        map.put(TbsCoreSettings.TBS_SETTINGS_USE_DEXLOADER_SERVICE, true);
        QbSdk.initTbsSettings(map);
        QbSdk.PreInitCallback cb = new QbSdk.PreInitCallback() {
            @Override
            public void onViewInitFinished(boolean arg0) {
                //x5內核初始化完成的回调，为true表示x5内核加载成功，否则表示x5内核加载失败，会自动切换到系统内核。
                CommonUtils.INSTANCE.log("app: onViewInitFinished is " + arg0);
            }

            @Override
            public void onCoreInitFinished() {
            }
        };
        //x5内核初始化接口
        QbSdk.initX5Environment(getApplicationContext(), cb);
    }
}
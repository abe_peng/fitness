var bg_color = "#202023";
var white = "#FFFFFF";
var yellow = "#FFB902";
var green = "#2DD8B6";
var txt_6 = "#666666";
var txt_9 = "#999999";

function showLine(xJson, yJson, heightRatio) {
	formatHeight('chart', heightRatio);
	var x = JSON.parse(xJson);
	var y = JSON.parse(yJson);
	showLineChart(x, y);
}

function showLineChart(x, y) {
	var myChart = echarts.init(document.getElementById('chart'));
	// 指定图表的配置项和数据
	var option = {
		title: {
			padding: 10,
			textStyle: {
				fontSize: 12,
			},
		},
		backgroundColor: bg_color,
		xAxis: ballXAxis(x),
		yAxis: ballYAxis(),
		dataZoom: ballDataZoom(x),
		series: [{
			name: '',
			type: 'line',
			smooth: true,
			itemStyle: {
				normal: {
					//这里是重点
					color: yellow,
					areaStyle: {
						color: 'pink'
					},
					label: {
						show: false,
						textStyle: {
							color: white,
							fontSize: 18,
						},
						formatter: function(params) {
							// var texts = [];
							// return texts;
							if (params.value) { //如果当前值存在则拼接
								return params.value + 'cm'
							} else { //否则返回个空
								return '';
							}
						},
					},

				}
			},
			data: y
		}],
		grid: {
			x: 10,
			y: 20,
			x2: 10,
			y2: 30
		},
	};
	myChart.setOption(option, true);
	setChartEnd();
}

function ballXAxis(x) {
	return {
		data: x,
		boundaryGap: true,
		axisLine: {
			show: true,
			lineStyle: { // 属性lineStyle控制线条样式
				color: txt_9,
				width: 3,
			},
		},
		axisLabel: {
			show: true,
			textStyle: {
				color: white, //更改坐标轴文字颜色
				fontSize: 18, //更改坐标轴文字大小
			},
			//interval: 0, //代表显示所有x轴标签显示
			//rotate: 45, //代表逆时针旋转45度
		},
	}
}

function ballYAxis() {
	return [{
		show: true,
		max: 500,
		axisLine: {
			show: false,
			lineStyle: { // 属性lineStyle控制线条样式
				color: txt_9,
				width: 3,
			},
		},
		axisTick: {
			show: false
		},
		splitLine: {
			show: false
		},
		type: 'value',
		axisLabel: {
			formatter: function(value) {
				var texts = [];
				return texts;
			},
			textStyle: {
				color: txt_9
			}
		},
		splitArea: {
			show: false
		}
	}];
}

function ballDataZoom(x) {
	var endTemp = 10 * 100 / x.length;
	endTemp = endTemp > 100 ? 100 : endTemp;
	return {
		type: 'inside',
		preventDefaultMouseMove: false,
		start: 100 - endTemp,
		end: 100
	};
}

var baseUrl = "http://192.168.0.175:8080/";
var themeColor = "#0099FF";
var white = "#FFFFFF";
var txt_d = "#DDDDDD";

//判断字符是否为空的方法
function isEmpty(obj) {
	if (typeof obj == "undefined" || obj == null) {
		return true;
	} else {
		return false;
	}
}

function stringEmpty(string) {
	return isEmpty(string) || string == "";
}

function compareTime(a, b) {
	var arr = a.split("-"); //log [2016,04,06]
	var start = new Date(arr[0], (arr[1] - 1), arr[2]);
	var starts = start.getTime(); //输出时间戳进行对比
	var arrs = b.split("-");
	var end = new Date(arrs[0], (arrs[1] - 1), arrs[2]);
	var ends = end.getTime();
	return ends >= starts;
}

function formatHeight(chartId, heightRatio) {
	if (heightRatio > 0) {
		var height = heightRatio * window.screen.height;
		var chart = document.getElementById(chartId);
		chart.style.height = height + 'px';
	}
}

function setChartEnd(data) {
	window.JsHandler.chartJsCall(data);
}
